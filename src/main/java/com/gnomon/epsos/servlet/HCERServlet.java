package com.gnomon.epsos.servlet;

import com.gnomon.epsos.hcer.HCERDocs;
import com.gnomon.epsos.hcer.HcerPersistentManager;
import com.gnomon.epsos.service.EpsosHelperService;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.model.User;
import epsos.ccd.gnomon.xslt.EpsosXSLTransformer;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Level;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.orm.PersistentException;
import org.orm.PersistentSession;

public class HCERServlet extends HttpServlet {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private static Logger log = Logger.getLogger(CDAServlet.class.getName());

    public void doGet(HttpServletRequest req,
            HttpServletResponse res)
            throws ServletException, IOException {

        String hcerdocid = ParamUtil.getString(req, "uuid");
        String exporttype = ParamUtil.getString(req, "exporttype");
        HttpSession session = req.getSession();
        User user = (User) session.getAttribute("user");
        log.info("USER " + user.getEmailAddress());

        PersistentSession s = null;

        log.info("getting html document");
        try {
            s = HcerPersistentManager.instance().getSession();
            String lang = user.getLanguageId();
            log.info("Language is " + lang);
            String lang1 = lang.replace("_", "-");
            lang1 = lang1.replace("en-US", "en");

            log.info("Portal language is : " + lang + " - " + lang1);
            HCERDocs doc = HCERDocs.getHCERDocsByORMID(s, Integer.parseInt(hcerdocid));
            String xmlfile = doc.getDocumentbody();
            log.info("#### CDA XML Start");
            log.info(xmlfile);
            log.info("#### CDA XML End");
            boolean isCDA = EpsosHelperService.isCDA(EpsosHelperService.loadXMLFromString(xmlfile));
            String cda = "";
            if (exporttype.equals("xml")) {
                res.setHeader("Content-Disposition", "attachment; filename=cda.xml");
                res.setContentType("text/xml");
                cda = xmlfile;
            } else {
                res.setContentType("text/html");
                EpsosXSLTransformer xlsClass = new EpsosXSLTransformer();
                cda = xlsClass.transform(xmlfile, lang1, "");

                if (isCDA) {
                    cda = xlsClass.transform(xmlfile, lang1, "");
                } else {
                    cda = xlsClass.transformUsingStandardCDAXsl(xmlfile);
                }
            }

            byte[] output = null;
            output = cda.getBytes();
            ByteArrayOutputStream baos = null;
            res.setHeader("Cache-Control", "no-cache");
            res.setDateHeader("Expires", 0);
            res.setHeader("Pragma", "No-cache");

            OutputStream OutStream = res.getOutputStream();
            OutStream.write(output);
            OutStream.flush();
            OutStream.close();

        } catch (Exception ex) {
            log.error(ExceptionUtils.getStackTrace(ex));
            res.setContentType("text/html");
            res.setHeader("Cache-Control", "no-cache");
            res.setDateHeader("Expires", 0);
            res.setHeader("Pragma", "No-cache");

            OutputStream OutStream = res.getOutputStream();
            OutStream.write(ex.getMessage().getBytes());
            OutStream.flush();
            OutStream.close();
            log.error(ExceptionUtils.getStackTrace(ex));
        } finally {
            try {
                s.close();
            } catch (PersistentException ex) {
                java.util.logging.Logger.getLogger(HCERServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }
}
